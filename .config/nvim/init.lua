-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ','
vim.g.maplocalleader = ','

-- [[ Install `lazy.nvim` plugin manager ]]
--    https://github.com/folke/lazy.nvim
--    `:help lazy.nvim.txt` for more info
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

--{{{ Plugins

-- [[ Configure plugins ]]
-- NOTE: Here is where you install your plugins.
--  You can configure plugins using the `config` key.
--
--  You can also configure plugins after the setup call,
--    as they will be available in your neovim runtime.
require('lazy').setup({
  -- NOTE: First, some plugins that don't require any configuration

  -- Git related plugins
  'tpope/vim-fugitive',

  -- NOTE: This is where your plugins related to LSP can be installed.
  --  The configuration is done below. Search for lspconfig to find it below.
  {
    -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      { 'williamboman/mason.nvim', config = true },
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
      -- { 'j-hui/fidget.nvim', opts = {} },

      -- Additional lua configuration, makes nvim stuff amazing!
      'folke/neodev.nvim',
    },
  },

  {
    -- Autocompletion
    'hrsh7th/nvim-cmp',
    dependencies = {
      -- Snippet Engine & its associated nvim-cmp source
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip',

      -- Adds LSP completion capabilities
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-path',

      -- Adds a number of user-friendly snippets
      'rafamadriz/friendly-snippets',
    },
  },
  -- Gitlab integration
  {
    "harrisoncramer/gitlab.nvim",
    dependencies = {
      "MunifTanjim/nui.nvim",
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
      "stevearc/dressing.nvim",     -- Recommended but not required. Better UI for pickers.
      "nvim-tree/nvim-web-devicons" -- Recommended but not required. Icons in discussion tree.
    },
    enabled = true,
    build = function() require("gitlab.server").build(true) end, -- Builds the Go binary
    config = function()
      require("gitlab").setup()
    end,
  },
  -- Useful plugin to show you pending keybinds.
  { 'folke/which-key.nvim',  opts = {} },
  {
    -- Adds git related signs to the gutter, as well as utilities for managing changes
    'lewis6991/gitsigns.nvim',
    opts = {
      current_line_blame = true,

      current_line_blame_opts = {
        virt_text = true,
        virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
        hl_mode = 'combine',
        delay = 500,
        ignore_whitespace = false,
      },

      current_line_blame_formatter_opts = {
        relative_time = true
      }
    },
  },
  {
    'stevearc/oil.nvim',
    opts = {},
    -- Optional dependencies
    dependencies = { "nvim-tree/nvim-web-devicons" },
  },
  {
    -- Theme and coloring
    'sainnhe/gruvbox-material',
    priority = 1000,
    config = function()
      vim.cmd.colorscheme 'gruvbox-material'
    end,
  },

  'xiyaowong/nvim-transparent',

  'tpope/vim-projectionist',
  {
    "ahmedkhalf/project.nvim",
    config = function()
      require("project_nvim").setup {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      }
    end
  },
  'tpope/vim-surround',

  {
    'cappyzawa/trim.nvim',
    config = function()
      require("trim").setup({
        trim_on_write = false,
        trim_trailing = true,
        trim_last_line = true,
        trim_first_line = true,
      })
    end
  },
  -- lazy.nvim
  {
    "folke/noice.nvim",
    event = "VeryLazy",
    opts = {
      -- add any options here
    },
    dependencies = {
      -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
      "MunifTanjim/nui.nvim",
    }
  },
  {
    -- Set lualine as statusline
    'nvim-lualine/lualine.nvim',
    -- See `:help lualine.txt`
    opts = {
      options = {
        icons_enabled = true,
        theme = 'gruvbox-material',
        component_separators = { left = '', right = '' },
        section_separators = { left = '', right = '' },
        disabled_filetypes = {
          statusline = {},
          winbar = {},
        },
        ignore_focus = {},
        always_divide_middle = true,
        globalstatus = false,
        refresh = {
          statusline = 1000,
          tabline = 1000,
          winbar = 1000,
        }
      },
      sections = {
        lualine_a = { 'mode' },
        lualine_b = { 'branch', 'diff', 'diagnostics' },
        lualine_c = { 'filename' },
        lualine_x = { 'encoding', 'fileformat', 'filetype' },
        lualine_z = { 'location' }
      },
    },
  },

  -- "gc" to toggle comment visual regions/lines
  { 'numToStr/Comment.nvim', opts = {} },

  -- Fuzzy Finder (files, lsp, etc)
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope-symbols.nvim',
      -- Fuzzy Finder Algorithm which requires local dependencies to be built.
      -- Only load if `make` is available. Make sure you have the system
      -- requirements installed.
      {
        'nvim-telescope/telescope-fzf-native.nvim',
        -- NOTE: If you are having trouble with this installation,
        --       refer to the README for telescope-fzf-native for more instructions.
        build = 'make',
        cond = function()
          return vim.fn.executable 'make' == 1
        end,
      },
    },
  },

  {
    -- Highlight, edit, and navigate code
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
    },
    build = ':TSUpdate',
  },
}, {})

--}}}

--{{{ Sets

-- [[ Setting options ]]
-- See `:help vim.o`
-- NOTE: You can change these options as you wish!

-- Set folds to require markers
vim.wo.foldmethod = 'marker'

-- Set color column
vim.wo.colorcolumn = '98'

-- Control the indentation
vim.o.smartindent = true
vim.o.expandtab = true
vim.o.tabstop = 2
vim.o.shiftwidth = 2

-- Keeps the cursor centered on the screen
vim.wo.scrolloff = 999

-- Make line numbers default
vim.wo.number = true
vim.wo.relativenumber = true

-- Set statusbar to show only global
vim.o.laststatus = 3

-- Enable mouse mode
vim.o.mouse = nil

-- Enable break indent
vim.o.breakindent = true

-- Save undo history
vim.o.undofile = false

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = "menu,menuone,noselect"

-- NOTE: You should make sure your terminal supports this
vim.o.termguicolors = true

-- Set shortmess to not show ins-completion-menu messages
vim.o.shortmess = "filnxtToOfc"

--}}}

--{{{ Plugins configuration

-- [[ Configure Telescope ]]
-- See `:help telescope` and `:help telescope.setup()`
require('telescope').setup {
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = false,
        ['<C-d>'] = false,
      },
    },
  },
}

-- Disable notifications
require('noice').setup({
  background_colour = "#000000",
  messages = {
    enabled = false
  }
})

-- oil.nvim 
require('oil').setup({})

-- Enable telescope fzf native, if installed
pcall(require('telescope').load_extension, 'fzf')

-- [[ Configure Treesitter ]]
-- See `:help nvim-treesitter`
-- Defer Treesitter setup after first render to improve startup time of 'nvim {filename}'
vim.defer_fn(function()
  require('nvim-treesitter.configs').setup {
    -- Add languages to be installed here that you want installed for treesitter
    ensure_installed = { 'lua', 'vimdoc', 'vim' },

    modules = {},

    ignore_install = {},

    sync_install = false,

    -- Autoinstall languages that are not installed. Defaults to false (but you can change for yourself!)
    auto_install = true,

    highlight = { enable = true },
    indent = { enable = true },
    incremental_selection = { enable = true },
  }
end, 0)

--}}}

--{{{ Autocommands

vim.api.nvim_create_autocmd('BufWritePost', {
  group = vim.api.nvim_create_augroup("Notetaker", { clear = true }),
  pattern = { '*note-*.md' },
  command = '! createnotes %:p',
})

--}}}

--{{{ Key mappings

-- [[ Basic Keymaps ]]

-- Fileexplorer mapping
vim.keymap.set("n", "-",  '<cmd>Oil --float<cr>')

local gitlab = require("gitlab")
require("gitlab.server")
vim.keymap.set("n", "glr", gitlab.review)
vim.keymap.set("n", "gls", gitlab.summary)
vim.keymap.set("n", "glA", gitlab.approve)
vim.keymap.set("n", "glR", gitlab.revoke)
vim.keymap.set("n", "glc", gitlab.create_comment)
vim.keymap.set("v", "glc", gitlab.create_multiline_comment)
vim.keymap.set("v", "glC", gitlab.create_comment_suggestion)
vim.keymap.set("n", "glO", gitlab.create_mr)
vim.keymap.set("n", "glm", gitlab.move_to_discussion_tree_from_diagnostic)
vim.keymap.set("n", "gln", gitlab.create_note)
vim.keymap.set("n", "gld", gitlab.toggle_discussions)
vim.keymap.set("n", "glaa", gitlab.add_assignee)
vim.keymap.set("n", "glad", gitlab.delete_assignee)
vim.keymap.set("n", "glra", gitlab.add_reviewer)
vim.keymap.set("n", "glrd", gitlab.delete_reviewer)
vim.keymap.set("n", "glp", gitlab.pipeline)
vim.keymap.set("n", "glo", gitlab.open_in_browser)
vim.keymap.set("n", "glM", gitlab.merge)

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- move line where cursor is either up or down
vim.keymap.set('n', '<a-j>', '<cmd>m +1<cr>==', { noremap = true })
vim.keymap.set('n', '<a-k>', '<cmd>m -2<cr>==', { noremap = true })
vim.keymap.set('i', '<a-j>', '<esc><cmd>m +1<cr>==gi', { noremap = true })
vim.keymap.set('i', '<a-k>', '<esc><cmd>m -2<cr>==gi', { noremap = true })
--
-- Keep selected when indenting text after each motion
vim.keymap.set('v', '<', '<gv', { noremap = true })
vim.keymap.set('v', '>', '>gv', { noremap = true })

-- Telescope live_grep in git root
-- Function to find the git root directory based on the current buffer's path
-- <leader>c is code
-- <leader>d is diff
-- <leader>e is editor
-- <leader>f is find
-- <leader>g is git
-- <leader>v is vim
-- <leader>w is window
--
-- document existing key chains
require('which-key').register {
  ['<leader>c'] = { name = '[C]ode', _ = 'which_key_ignore' },
  ['<leader>d'] = { name = '[D]diff', _ = 'which_key_ignore' },
  ['<leader>e'] = { name = '[E]ditor', _ = 'which_key_ignore' },
  ['<leader>f'] = { name = '[F]ind', _ = 'which_key_ignore' },
  ['<leader>g'] = { name = '[G]it', _ = 'which_key_ignore' },
  ['<leader>v'] = { name = '[V]im', _ = 'which_key_ignore' },
  ['<leader>w'] = { name = '[W]indow', _ = 'which_key_ignore' },
}

-- <leader>f is find followed by:
-- b = open buffers
-- r = recent files
-- f = files
-- g = git files
-- G = grep files
-- t = tags
-- d = diagnostics
vim.keymap.set('n', '<leader>fr', require('telescope.builtin').oldfiles, { desc = '[F]ind [R]ecently opened files' })
vim.keymap.set('n', '<leader>fb', require('telescope.builtin').buffers, { desc = '[F]ind existing [B]uffers' })
vim.keymap.set('n', '<leader>ff', require('telescope.builtin').find_files, { desc = '[F]ind [F]iles' })
vim.keymap.set('n', '<leader>fg', require('telescope.builtin').git_files,
  { desc = '[F]ind files in the [G]it repository' })
vim.keymap.set('n', '<leader>fp', require('telescope').extensions.projects.projects, { desc = '[F]ind [P]rojects' })
vim.keymap.set('n', '<leader>ft', require('telescope.builtin').tags, { desc = '[F]ind [T]ags' })
vim.keymap.set('n', '<leader>fG', require('telescope.builtin').live_grep, { desc = '[F]ind by [G]rep' })
vim.keymap.set('n', '<leader>fd', require('telescope.builtin').diagnostics, { desc = '[F]find [D]iagnostics' })

-- <leader>w is for window
-- j = moves cursor to window below
-- k = moves cursor to window above
-- l = moves cursor to window to the right
-- h = moves cursor to window to the left
-- u = increase window size by 5
-- > = increase window size by 5
-- d = decrease window size by 5
-- < = decrease window size by 5
vim.keymap.set('n', '<leader>wj', '<c-w>j', {})
vim.keymap.set('n', '<leader>wk', '<c-w>k', {})
vim.keymap.set('n', '<leader>wl', '<c-w>l', {})
vim.keymap.set('n', '<leader>wh', '<c-w>h', {})
vim.keymap.set('n', '<leader>wu', '<cmd>resize +5<cr>', {})
vim.keymap.set('n', '<leader>wd', '<cmd>resize -5<cr>', {})
vim.keymap.set('n', '<leader>w>', '<cmd>vertical resize +5<cr>', {})
vim.keymap.set('n', '<leader>w<', '<cmd>vertical resize -5<cr>', {})

-- <leader>d is for diff
-- s = create 2 way split window
-- S = create 3 way split window for merging
-- g = executes diffget
-- p = executes diffput
-- h = executes diffget from the index perspective
-- l = executes diffget from the working perspective
vim.keymap.set('n', '<leader>ds', '<cmd>Gvdiffsplit<cr>', {})
vim.keymap.set('n', '<leader>dS', '<cmd>Gvdiffsplit!<cr>', {})
vim.keymap.set('n', '<leader>dg', '<cmd>diffget<cr>', {}) -- same as `do` without leader
vim.keymap.set('n', '<leader>dp', '<cmd>diffput<cr>', {}) -- same as `dp`
vim.keymap.set('n', '<leader>dh', '<cmd>diffget //2 | diffupdate<cr>', {})
vim.keymap.set('n', '<leader>dl', '<cmd>diffget //3 | diffupdate<cr>', {})

-- <leader>e is for editor
-- s = Show any/all markdown symbols with telescopy
-- a = Jumpt to alternate file of the current file
-- c = Copy/paste to system clipboard
-- d = Show floating diagnostics window for the current diagnostic
-- D = Show diagnostics window
-- n = Goto/jump to next diagnostics issue
-- N = Goto/jump to previous diagnostics issue
-- o = Close all windows leaving open only the focused window
-- f = Toggle folding of code
-- w = Strip whitepace
vim.keymap.set('n', '<leader>es', "<cmd>Telescope symbols<cr>", { desc = "[E]ditor markdown [S]ymbols" })
vim.keymap.set('n', '<leader>ea', "<cmd>A<cr>", { desc = "[E]ditor [A]lternate file" })
vim.keymap.set('n', '<leader>ec', "<cmd>w !xclip -silent -sel c<cr><cr>", { desc = "[E]ditor [C]opy/paste" })
vim.keymap.set('v', '<leader>ec', ":w !xclip -silent -sel c<cr><cr>", { desc = "[E]ditor [C]opy/paste" })
vim.keymap.set('n', '<leader>ed', vim.diagnostic.open_float, { desc = "[E]ditor open floating diagnostic window" })
vim.keymap.set('n', '<leader>eD', vim.diagnostic.setloclist, { desc = "[E]ditor open [D]iagnostic window" })
vim.keymap.set('n', '<leader>en', vim.diagnostic.goto_next, { desc = "[E]ditor [n]ext diagnostic issue" })
vim.keymap.set('n', '<leader>eN', vim.diagnostic.goto_prev, { desc = "[E]ditor [p]revious diagnostic issue" })
vim.keymap.set('n', '<leader>eo', "<cmd>only<cr>", { desc = "[E]ditor keep open on [O]nly focused window" })
vim.keymap.set('n', '<leader>ew', "<cmd>Trim<cr>", { desc = "[E]ditor Strip[W]hitepace" })
vim.keymap.set('n', '<leader>ef', 'za', { desc = '[C]ode [F]old' })

-- <leader>g is git
-- s = view status
-- b = select branch
-- k = select commit
-- h = select stash
-- c = create new commit
-- r = read current file from index
-- w = write current file from index
vim.keymap.set('n', '<leader>gs', '<cmd>Gs:<cr>', {})
vim.keymap.set('n', '<leader>gb', '<cmd>Telescope git_branches<cr>', {})
vim.keymap.set('n', '<leader>gk', '<cmd>Telescope git_commits<cr>', {})
vim.keymap.set('n', '<leader>gh', '<cmd>Telescope git_stash<cr>', {})
vim.keymap.set('n', '<leader>gc', '<cmd>G commit<cr>', {})
vim.keymap.set('n', '<leader>gr', '<cmd>Gread<cr>', {})
vim.keymap.set('n', '<leader>gw', '<cmd>Gwrite<cr>', {})

-- <leader>v is vim commands
-- i = edit init.lua file
-- b = edit bashrc file
-- k = edit sxhkd file for kep mappings
-- p = edit project_history file
vim.keymap.set('n', '<leader>vi', "<cmd>edit ~/.config/nvim/init.lua<cr>", {})
vim.keymap.set('n', '<leader>vb', "<cmd>edit ~/.bashrc<cr>", {})
vim.keymap.set('n', '<leader>vk', "<cmd>edit ~/.config/sxhkd/sxhkdrc<cr>", {})
vim.keymap.set('n', '<leader>vp', "<cmd>edit ~/.local/share/nvim/project_nvim/project_history<cr>", {})

-- [[ Configure LSP ]]
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)
  -- NOTE: Remember that lua is a real programming language, and as such it is possible
  -- to define small helper and utility functions so you don't have to repeat yourself
  -- many times.
  --
  -- In this case, we create a function that lets us more easily define mappings specific
  -- for LSP related items. It sets the mode, buffer and description for us each time.
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  -- <leader>c is code commands
  -- r = Rename the current file
  -- a = Display code actions for diagnostics
  -- f = Format the current buffer
  -- d = Display list of code definations
  -- r = Display list of code references
  -- i = Display list of code implementations
  -- D = Display code Type Defination
  -- s = Display document symbols
  -- S = Display workspace symbols
  -- e = Display code declaration
  -- w = Add new workspace folder
  -- W = Remove workspace folder
  -- W = Diplay list of workspace folders
  nmap('<leader>cr', vim.lsp.buf.rename, '[Code] [R]ename')
  nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')
  nmap('<leader>cf', vim.lsp.buf.format, '[C]ode [F]ormat current buffer')
  nmap('<leader>cd', require('telescope.builtin').lsp_definitions, '[C]ode [D]efinition')
  nmap('<leader>cr', require('telescope.builtin').lsp_references, '[C]ode [R]eferences')
  nmap('<leader>ci', require('telescope.builtin').lsp_implementations, '[C]ode [I]mplementation')
  nmap('<leader>cD', require('telescope.builtin').lsp_type_definitions, '[C]ode Type [D]efinition')
  nmap('<leader>cs', require('telescope.builtin').lsp_document_symbols, '[C]ode document [S]ymbols')
  nmap('<leader>cS', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[C]ode workspace [S]ymbols')
  nmap('<leader>ce', vim.lsp.buf.declaration, '[C]ode d[E]claration')
  nmap('<leader>cw', vim.lsp.buf.add_workspace_folder, '[C]ode add [W]orkspace Folder')
  nmap('<leader>cW', vim.lsp.buf.remove_workspace_folder, '[C]ode remove [W]orkspace Folder')
  nmap('<leader>cL', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[C]ode workspace [L]ist of Folders')

  -- Special mappings
  -- K will show documentation
  -- C-K will show signature help
  nmap('K', vim.lsp.buf.hover, 'Hover Do[K]umentation') -- See `:help K` for why this keymap
  nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')


  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    vim.lsp.buf.format()
  end, { desc = 'Format current buffer with LSP' })
end

--}}}

--{{{ LSP setup & configuration

-- mason-lspconfig requires that these setup functions are called in this order
-- before setting up the servers.
require('mason').setup()
require('mason-lspconfig').setup()

-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
--
--  If you want to override the default filetypes that your language server will attach to you can
--  define the property 'filetypes' to the map in question.
local servers = {
  -- clangd = {},
  -- gopls = {},
  -- pyright = {},
  -- rust_analyzer = {},
  -- tsserver = {},
  -- html = { filetypes = { 'html', 'twig', 'hbs'} },

  lua_ls = {
    Lua = {
      workspace = { checkThirdParty = false },
      telemetry = { enable = false },
      -- NOTE: toggle below to ignore Lua_LS's noisy `missing-fields` warnings
      -- diagnostics = { disable = { 'missing-fields' } },
    },
  },
}

-- Setup neovim lua configuration
require('neodev').setup()

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
  function(server_name)
    require('lspconfig')[server_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
      filetypes = (servers[server_name] or {}).filetypes,
    }
  end,
}

-- [[ Configure nvim-cmp ]]
-- See `:help cmp`
local cmp = require 'cmp'
local luasnip = require 'luasnip'
require('luasnip.loaders.from_vscode').lazy_load()
luasnip.config.setup {}

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  completion = {
    completeopt = 'menu,menuone,noinsert',
  },
  mapping = cmp.mapping.preset.insert {
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete {},
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_locally_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.locally_jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  },
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
    { name = 'path' },
  },
}

--}}}

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
