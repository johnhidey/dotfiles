#!/bin/bash
#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

FILES=(
  "${HOME}/.bash_exports"
  "${HOME}/.bash_aliases"
  "${XDG_DATA_HOME}/asdf/asdf.sh"
  "${XDG_DATA_HOME}/asdf/completions/asdf.bash"
  "${KITTY_INSTALLATION_DIR}/shell-integration/bash/kitty.bash"
  "${XDG_CONFIG_HOME}/op/plugins.sh"
  "${XDG_DATA_HOME}/asdf/plugins/java/set-java-home.bash"
)

for file in "${FILES[@]}"
do
  [[ -f "${file}" ]] && echo "Sourcing $(basename "${file}")" && source "${file}";
done
