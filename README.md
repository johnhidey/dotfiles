## My dotfiles

### Prereqs

* Fira Code Font
* Papirus Icon Theme
* ASDF (Use asdf for all development tools that I can)

### Current configs

* bash 
* bspwm
* dunst
* kitty (terminal)
* neovim 
* rofi (launcher)
* sxhkd
