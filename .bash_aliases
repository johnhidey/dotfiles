#!/bin/bash

alias ls='lsd -lhF --color=auto'
alias ll='lsd -alhF --color=auto'
alias rm='rm -rf -i'
alias dirs='dirs -v'
alias pd='pushd'
alias search='googler'
alias kssh='kitty +kitten ssh'
alias psql='PAGER="less -S" psql'

# Git aliases
#
# git log short
alias gls='git log --graph --pretty=format:"%C(magenta)%h%Creset -%C(red)%d%Creset %s %C(dim green)(%cr) [%an]" --abbrev-commit -30'
# git long log
alias gll='git log --abbrev-commit'
# git fixup commit
alias gcf='git commit --fixup=$(select_commit)'

alias dotfiles='git --git-dir=$HOME/repos/dotfiles --work-tree=$HOME'
alias md='mdcat -p'
alias cat='bat'
alias please='sudo $(fc -ln -1)'
alias open='xdg-open'
alias vi='nvim'
alias vim='nvim'
alias vifm='vifmrun'
alias view='nvim -R'
alias virsh='virsh --connect qemu:///system'

